from tkinter import *
import tkinter.messagebox as tm
import json
import urllib3
import requests


class Messenger:
    def __init__(self, ser):
        self.ser = ser

    def send_rec(self, msg):
        self.ser.write((msg + "\n").encode())
        return self.ser.read_until(b"\n", 255)#.encode()

    def send_message(self,message):
        print(message)
        self.send_rec(message)

    def get_status(self):
        self.send_rec('status')

# class StartingFrame(Frame):
#
#


class LoginFrame(Frame):
    def __init__(self, master):
        super().__init__(master)

        self.label_username = Label(self, text="Username")
        self.label_password = Label(self, text="Password")
        self.label_confirmPassword = Label(self, text="Confirm Password")
        self.label_firstname = Label(self, text="First Name")
        self.label_lastname = Label(self, text="Last Name")
        self.label_email = Label(self, text="Email")

        self.entry_username = Entry(self)
        self.entry_password = Entry(self, show="*")
        self.entry_confirmPassword = Entry(self, show="*")
        self.entry_firstname = Entry(self)
        self.entry_lastname = Entry(self)
        self.entry_email = Entry(self)

        self.label_username.grid(row=0, sticky=E)
        self.entry_username.grid(row=0, column=1)
        self.label_password.grid(row=1, sticky=E)
        self.entry_password.grid(row=1, column=1)
        self.label_confirmPassword.grid(row=2, sticky=E)
        self.entry_confirmPassword.grid(row=2, column=1)
        self.label_firstname.grid(row=3, sticky=E)
        self.entry_firstname.grid(row=3, column=1)
        self.label_lastname.grid(row=4, sticky=E)
        self.entry_lastname.grid(row=4, column=1)
        self.label_email.grid(row=5, sticky=E)
        self.entry_email.grid(row=5, column=1)

        self.logbtn = Button(self, text="Login", command=self._login_btn_clicked)
        self.logbtn.grid(columnspan=2)

        self.pack()

    def _login_btn_clicked(self):

        url = 'http://localhost:8080/api/register'

        data = {
            'username': self.entry_username.get(),
            'password': self.entry_password.get(),
            'repeatPassword': self.entry_confirmPassword.get(),
            'email': self.entry_email.get(),
            'firstName': self.entry_firstname.get(),
            'lastName': self.entry_lastname
        }

        r = requests.post(url,data)
        status = r.status_code

        if status == requests.codes.ok:
            tm.showinfo("Register Success")
        else:
            tm.showerror("Something's wrong, please try again")

if __name__ == '__main__':
    root = Tk()
    root.title("Register LayzLiving Account")
    root.geometry("700x500")
    lf = LoginFrame(root)
    root.mainloop()
