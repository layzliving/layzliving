from tkinter import *
import tkinter.messagebox as tm
import json
import urllib3
import requests


class Messenger:
    def __init__(self, ser):
        self.ser = ser

    def send_rec(self, msg):
        self.ser.write((msg + "\n").encode())
        return self.ser.read_until(b"\n", 255)#.encode()

    def send_message(self,message):
        print(message)
        self.send_rec(message)

    def get_status(self):
        self.send_rec('status')

# class StartingFrame(Frame):
#
#


class LoginFrame(Frame):
    def __init__(self, master):
        super().__init__(master)

        self.lbl_mqtt_host = Label(self, text="Mosquitto Hostname")
        self.lbl_mqtt_user = Label(self, text="Username")
        self.lbl_mqtt_pass = Label(self, text="Password")

        self.etn_host = Entry(self)
        self.etn_user = Entry(self)
        self.etn_pass = Entry(self,show="*")

        self.lbl_mqtt_host.grid(row=0, sticky=E)
        self.etn_host.grid(row=0, column=1)
        self.lbl_mqtt_user.grid(row=1,sticky=E)
        self.etn_user.grid(row=1,column=1)
        self.lbl_mqtt_pass.grid(row=2, sticky=E)
        self.etn_pass.grid(row=2, column=1)


        self.btn_conn = Button(self, text="Submit", command=self._setup_mqtt)
        self.btn_conn.grid(columnspan=2)

        self.pack()

    def _setup_mqtt(self):
        print(self.etn_user.get())
        print(self.etn_host.get())
        print(self.etn_pass.get())

if __name__ == '__main__':
    root = Tk()
    root.title("Config the house")
    root.geometry("700x500")
    lf = LoginFrame(root)
    root.mainloop()
